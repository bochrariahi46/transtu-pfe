package tn.transtu.pfe.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="extincteur")
public class Extincteur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_extincteur")
    private int id;
    @Column(name="ref")
    private String ref;
    @Column(name="emplacement")
    private String emplacement;
    @Column(name = "Date_entretien")
    private String dateEnt;
}
