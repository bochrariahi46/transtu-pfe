package tn.transtu.pfe.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"pointage"})
@Entity
@Table(name="utilisateur")
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user" )
    private int id;
    @Column(name = "matricule" )
    private int matricule;
    @Column(name = "role")
    private String role;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "tél")
    private int tel;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @ManyToOne
    @JoinColumn(name = "id_pointage",referencedColumnName = "id_pointage")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Pointage pointage;

}
