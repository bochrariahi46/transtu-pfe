package tn.transtu.pfe.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="pointage")
public class Pointage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pointage" )
    private int id;
    @Column(name="Temps")
    private String temps;
    @OneToMany(mappedBy = "pointage")
    private List<Utilisateur> utilisateurs;
}
