package tn.transtu.pfe.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name="reclamation")
public class Reclamation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reclamation" )
    private int id;
    @Column(name="lieu")
    private String lieu;
    @Column(name="heure")
    private String heure;
    @Column(name = "numbus")
    private String numBus;

}
