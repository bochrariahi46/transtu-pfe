package tn.transtu.pfe.services;

import tn.transtu.pfe.entities.Informateur;
import tn.transtu.pfe.models.InformateurModels;

import java.util.List;

public interface InformateurService {
    Informateur create(InformateurModels i);

    List<Informateur> getAll();
}
