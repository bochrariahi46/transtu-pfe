package tn.transtu.pfe.services;

import tn.transtu.pfe.entities.Utilisateur;
import tn.transtu.pfe.models.UtilisateurModels;

import java.util.List;

public interface UtilisateurService {
    Utilisateur create(UtilisateurModels u);

    Utilisateur update(int id, UtilisateurModels u);

    void deleteById(int id);

    Utilisateur updatePassword(int id, UtilisateurModels u);

    List<Utilisateur> getAll();

    List<Utilisateur> searchByMatUs(int matricule);
}
