package tn.transtu.pfe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.transtu.pfe.entities.Informateur;
import tn.transtu.pfe.entities.Utilisateur;
import tn.transtu.pfe.models.InformateurModels;
import tn.transtu.pfe.repositories.InformateurRepository;

import java.util.List;

@Service
public class InformateurServiceImpl implements InformateurService {
    @Autowired
    private InformateurRepository informateurRepository;

    @Override
    public Informateur create(InformateurModels i) {
        Informateur in = new Informateur();
        in.setAdjective(i.getAdjective());
        in.setFirstName(i.getFirstName());
        in.setLastName(i.getLastName());
        in.setTel(i.getTel());
        return informateurRepository.save(in);
    }
    @Override
    public List<Informateur> getAll(){
        return informateurRepository.findAll();

    }
}
