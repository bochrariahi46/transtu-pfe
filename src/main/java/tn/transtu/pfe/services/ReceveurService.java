package tn.transtu.pfe.services;

import tn.transtu.pfe.entities.Receveur;
import tn.transtu.pfe.models.ReceveurModels;

import java.util.List;

public interface ReceveurService {
    Receveur create(ReceveurModels r);

    Receveur update(int id, ReceveurModels r);

    List<Receveur> searchByMat(int matricule);

    List<Receveur> getAll();
}
