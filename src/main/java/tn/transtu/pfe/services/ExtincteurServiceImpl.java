package tn.transtu.pfe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tn.transtu.pfe.entities.Extincteur;
import tn.transtu.pfe.entities.Utilisateur;
import tn.transtu.pfe.models.ExtincteurModels;
import tn.transtu.pfe.models.UtilisateurModels;
import tn.transtu.pfe.repositories.ExtincteurRepository;

import java.util.List;

@Service
public class ExtincteurServiceImpl implements ExtincteurService {
    @Autowired
    private ExtincteurRepository extincteurRepository;

    @Override
    public Extincteur create(ExtincteurModels e) {
        Extincteur ex = new Extincteur();
        ex.setRef(e.getRef());
        ex.setEmplacement(e.getEmplacement());
        ex.setDateEnt(e.getDateEnt());

        return extincteurRepository.save(ex);
    }

    @Override
    public Extincteur update(int id, ExtincteurModels e) {
        Extincteur ex = extincteurRepository.getById(id);
        ex.setRef(e.getRef());
        ex.setEmplacement(e.getEmplacement());
        ex.setDateEnt(e.getDateEnt());

        return extincteurRepository.save(ex);
    }

    @Override
    public void deleteById(int id) {
        extincteurRepository.deleteById(id);
    }

    @Override
    public Extincteur updatePassword(int id, ExtincteurModels e) {
        Extincteur ex = extincteurRepository.getById(id);
        ex.setDateEnt(e.getDateEnt());
        return extincteurRepository.save(ex);
    }


    @Override
    public List<Extincteur> getAll() {
        return extincteurRepository.findAll();

    }

    @Override
    public List<Extincteur> getAllWithSorting(String field) {
        return extincteurRepository.findAll(Sort.by(Sort.Direction.ASC, field));

    }

    @Override
    public Page<Extincteur> getAllWithPagination(int offset, int pageSize) {
        Page<Extincteur> extincteurPage = extincteurRepository.findAll(PageRequest.of(offset, pageSize));
        return extincteurPage;
    }

    @Override
    public Page<Extincteur> getAllWithPaginationAndSorting(int offset, int pageSize,String field) {
        Page<Extincteur> extincteurPage = extincteurRepository.findAll(PageRequest.of(offset, pageSize).withSort(Sort.by(field)));
        return extincteurPage;
    }
    @Override
    public List<Extincteur> searchByRef(int ref) {
        return extincteurRepository.findByRef(ref);

    }
}
