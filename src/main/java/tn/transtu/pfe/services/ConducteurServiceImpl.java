package tn.transtu.pfe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.transtu.pfe.entities.Conducteur;
import tn.transtu.pfe.entities.Utilisateur;
import tn.transtu.pfe.models.ConducteurModels;
import tn.transtu.pfe.repositories.ConducteurRepository;

import java.util.List;

@Service
public class ConducteurServiceImpl implements ConducteurService {
    @Autowired
    private ConducteurRepository conducteurRepository;

    @Override
    public Conducteur create(ConducteurModels c) {
        Conducteur cd = new Conducteur();
        cd.setMatricule(c.getMatricule());
        cd.setFirstName(c.getFirstName());
        cd.setLastName(c.getLastName());
        cd.setTel(c.getTel());
        return conducteurRepository.save(cd);
    }

    @Override
    public Conducteur update(int id, ConducteurModels c) {
        Conducteur cd = conducteurRepository.getById(id);
        cd.setMatricule(c.getMatricule());
        cd.setFirstName(c.getFirstName());
        cd.setLastName(c.getLastName());
        cd.setTel(c.getTel());
        return conducteurRepository.save(cd);
    }
    @Override
    public List<Conducteur> searchByMat(int matricule){
        return conducteurRepository.findByMat(matricule);

    }
    @Override
    public List<Conducteur> getAll(){
        return conducteurRepository.findAll();

    }


}
