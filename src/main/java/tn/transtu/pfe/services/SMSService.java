package tn.transtu.pfe.services;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import tn.transtu.pfe.models.SmsPojo;


@Component

public class SMSService {


    private  String ACCOUNT_SID="AC75540fa43a62011ec475fb0dce93b19f" ;

    private  String AUTH_TOKEN="64a524c70682f268cc91c3495371a25c" ;

    private  String FROM_NUMBER="+16812903982";

    public void send(SmsPojo sms) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message.creator(new PhoneNumber(sms.getTo()), new PhoneNumber(FROM_NUMBER), sms.getMessage())
                .create();
        System.out.println("here is my id:"+message.getSid());//

    }

    public void receive(MultiValueMap<String, String> smscallback) {
    }

}