package tn.transtu.pfe.services;

import org.springframework.data.domain.Page;
import tn.transtu.pfe.entities.Extincteur;
import tn.transtu.pfe.models.ExtincteurModels;

import java.util.List;

public interface ExtincteurService {
    Extincteur create(ExtincteurModels e);

    Extincteur update(int id, ExtincteurModels e);

    void deleteById(int id);

    Extincteur updatePassword(int id, ExtincteurModels e);

    List<Extincteur> getAll();

    List<Extincteur> getAllWithSorting(String field);

    Page<Extincteur> getAllWithPagination(int offset, int pageSize);

    Page<Extincteur> getAllWithPaginationAndSorting(int offset, int pageSize, String field);

    List<Extincteur> searchByRef(int ref);
}
