package tn.transtu.pfe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.transtu.pfe.entities.Receveur;
import tn.transtu.pfe.models.ReceveurModels;
import tn.transtu.pfe.repositories.ReceveurRepository;

import java.util.List;
@Service
public class ReceveurServiceImpl implements ReceveurService{
    @Autowired
    private ReceveurRepository receveurRepository;
    @Override
    public Receveur create(ReceveurModels r) {
        Receveur rc = new Receveur();
        rc.setMatricule(r.getMatricule());
        rc.setFirstName(r.getFirstName());
        rc.setLastName(r.getLastName());
        rc.setTel(r.getTel());
        return receveurRepository.save(rc);
    }

    @Override
    public Receveur update(int id, ReceveurModels r) {
        Receveur rc = receveurRepository.getById(id);
        rc.setMatricule(r.getMatricule());
        rc.setFirstName(r.getFirstName());
        rc.setLastName(r.getLastName());
        rc.setTel(r.getTel());
        return receveurRepository.save(rc);
    }
    @Override
    public List<Receveur> searchByMat(int matricule){
        return receveurRepository.findByMat(matricule);

    }
    @Override
    public List<Receveur> getAll(){
        return receveurRepository.findAll();

    }


}
