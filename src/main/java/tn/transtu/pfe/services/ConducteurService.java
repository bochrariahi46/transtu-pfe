package tn.transtu.pfe.services;

import tn.transtu.pfe.entities.Conducteur;
import tn.transtu.pfe.models.ConducteurModels;

import java.util.List;

public interface ConducteurService {
    Conducteur create(ConducteurModels c);

    Conducteur update(int id, ConducteurModels c);

    List<Conducteur> searchByMat(int matricule);

    List<Conducteur> getAll();
}
