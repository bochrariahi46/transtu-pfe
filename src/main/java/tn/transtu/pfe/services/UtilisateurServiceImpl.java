package tn.transtu.pfe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.transtu.pfe.entities.Utilisateur;
import tn.transtu.pfe.models.UtilisateurModels;
import tn.transtu.pfe.repositories.UtilisateurRepository;

import java.util.List;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    @Override
    public Utilisateur create(UtilisateurModels u){
        Utilisateur us = new Utilisateur();
        us.setMatricule(u.getMatricule());
        us.setRole(u.getRole());
        us.setFirstName(u.getFirstName());
        us.setLastName(u.getLastName());
        us.setTel(u.getTel());
        us.setUsername(u.getUsername());
        us.setPassword(u.getPassword());
        return utilisateurRepository.save(us);
    }
    @Override
    public Utilisateur update(int id, UtilisateurModels u){
        Utilisateur us =  utilisateurRepository.getById(id);
        us.setMatricule(u.getMatricule());
        us.setRole(u.getRole());
        us.setFirstName(u.getFirstName());
        us.setLastName(u.getLastName());
        us.setTel(u.getTel());
        us.setUsername(u.getUsername());
        us.setPassword(u.getPassword());
        return utilisateurRepository.save(us);
    }

    @Override
    public void deleteById(int id){
        utilisateurRepository.deleteById(id);
    }

    @Override
    public Utilisateur updatePassword(int id, UtilisateurModels u){
        Utilisateur us = utilisateurRepository.getById(id);
        us.setPassword(u.getPassword());
        return utilisateurRepository.save(us);
    }


    @Override
    public List<Utilisateur> getAll(){
        return utilisateurRepository.findAll();

    }
    @Override
    public List<Utilisateur> searchByMatUs(int matricule){
        return utilisateurRepository.findByMat(matricule);

    }
}

