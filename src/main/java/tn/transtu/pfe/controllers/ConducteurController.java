package tn.transtu.pfe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.transtu.pfe.entities.Conducteur;
import tn.transtu.pfe.models.ConducteurModels;
import tn.transtu.pfe.services.ConducteurService;

import java.util.List;
@RestController
@RequestMapping(value = "/conducteur")
public class ConducteurController {
    @Autowired
    private ConducteurService conducteurService;
    @GetMapping
    public List<Conducteur> getAll(){

        return conducteurService.getAll();
    }
    @GetMapping(path = "/search/{matricule}")
    // liste des conducteurs
    public List<Conducteur> searchByMat(@PathVariable int matricule){
        return conducteurService.searchByMat(matricule);
    }

    @PostMapping(consumes = "application/json")
    public Conducteur addConducteur(@RequestBody ConducteurModels c){
        return conducteurService.create(c);
    }


    @PutMapping(path = "/{id}")
    public Conducteur update(@PathVariable int id,@RequestBody ConducteurModels c){
        return conducteurService.update(id,c);

    }
}


