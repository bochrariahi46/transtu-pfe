package tn.transtu.pfe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.transtu.pfe.entities.Receveur;
import tn.transtu.pfe.models.ReceveurModels;
import tn.transtu.pfe.services.ReceveurService;

import java.util.List;
@RestController
@RequestMapping(value = "/receveur")
public class ReceveurController {
    @Autowired
    private ReceveurService receveurService;
    @GetMapping
    public List<Receveur> getAll(){
        return receveurService.getAll();
    }
    @GetMapping(path = "/search/{matricule}")
    public List<Receveur> searchByMat(@PathVariable int matricule){
        return receveurService.searchByMat(matricule);
    }

    @PostMapping(consumes = "application/json")
    public Receveur addReceveur(@RequestBody ReceveurModels r){
        return receveurService.create(r);
    }


    @PutMapping(path = "/{id}")
    public Receveur update(@PathVariable int id,@RequestBody ReceveurModels r){
        return receveurService.update(id,r);

    }
}
