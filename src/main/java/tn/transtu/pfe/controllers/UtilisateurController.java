package tn.transtu.pfe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.transtu.pfe.entities.Utilisateur;
import tn.transtu.pfe.models.UtilisateurModels;
import tn.transtu.pfe.services.UtilisateurService;

import java.util.List;

@RestController
@RequestMapping(value = "/utilisateur")
public class UtilisateurController {
    @Autowired
    private UtilisateurService utilisateurService;
    @GetMapping
    public List<Utilisateur> getAll(){
        return utilisateurService.getAll();
    }
    @GetMapping(path = "/search/{matricule}")
    public List<Utilisateur> getByMat(@PathVariable int matricule){
        return utilisateurService.searchByMatUs(matricule);
    }


    @PostMapping(consumes = "application/json")
    public Utilisateur addAgent(@RequestBody UtilisateurModels u){
        return utilisateurService.create(u);
    }
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable int id){
        utilisateurService.deleteById(id);
    }

    @PatchMapping(path = "/{id}")
    public Utilisateur updatePassword(@PathVariable int id,@RequestBody UtilisateurModels u){
        return utilisateurService.updatePassword(id,u);

    }
}
