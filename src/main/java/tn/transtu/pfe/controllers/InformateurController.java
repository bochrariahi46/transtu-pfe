package tn.transtu.pfe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.transtu.pfe.entities.Informateur;
import tn.transtu.pfe.models.InformateurModels;
import tn.transtu.pfe.services.InformateurService;

import java.util.List;

@RestController
@RequestMapping(value = "/informateur")
public class InformateurController {
    @Autowired
    private InformateurService informateurService;

    @GetMapping
    public List<Informateur> getAll() {
        return informateurService.getAll();
    }

    @PostMapping(consumes = "application/json")
    public Informateur addInformateur(@RequestBody InformateurModels i) {
        return informateurService.create(i);
    }
}


