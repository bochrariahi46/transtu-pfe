package tn.transtu.pfe.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import tn.transtu.pfe.entities.Extincteur;
import tn.transtu.pfe.models.APIResponse;
import tn.transtu.pfe.models.ExtincteurModels;
import tn.transtu.pfe.services.ExtincteurService;

import java.util.List;

@RestController
@RequestMapping(value = "/extincteur")
public class ExtincteurController {
    @Autowired
    private ExtincteurService extincteurService;
    @GetMapping
    public APIResponse<List<Extincteur>> getAll(){
        List<Extincteur> extincteurList = extincteurService.getAll();
        return new APIResponse<>(extincteurList.size(), extincteurList);
    }
    //Sorting
    @GetMapping(path = "/{field}")
    public APIResponse<List<Extincteur>> getAllWithSorting(@PathVariable String field){
        List<Extincteur> extincteurSorting = extincteurService.getAllWithSorting(field);
        return new APIResponse<>(extincteurSorting.size(), extincteurSorting);
    }
    //Pagination
    @GetMapping(path = "/pagination/{offset}/{pageSize}")
    public APIResponse<Page<Extincteur>> getAllWithPagination(@PathVariable int offset,@PathVariable int pageSize){
        Page<Extincteur> extincteurWithPagination = extincteurService.getAllWithPagination(offset,pageSize);
        return new APIResponse<>(extincteurWithPagination.getSize(), extincteurWithPagination);
    }
    //filter
    @GetMapping(path = "/paginationAndSort/{offset}/{pageSize}/{field}")
    public APIResponse<Page<Extincteur>> getAllWithPaginationAndSorting(@PathVariable int offset,@PathVariable int pageSize,@PathVariable String field){
        Page<Extincteur> extincteurWithPaginationAndSorting = extincteurService.getAllWithPaginationAndSorting(offset,pageSize,field);
        return new APIResponse<>(extincteurWithPaginationAndSorting.getSize(), extincteurWithPaginationAndSorting);
    }


    @GetMapping(path = "/search/{ref}")
    public List<Extincteur> searchByRef(@PathVariable int ref){
        return extincteurService.searchByRef(ref);
    }

    @PostMapping(consumes = "application/json")
    public Extincteur addExtincteur(@RequestBody ExtincteurModels e){
        return extincteurService.create(e);
    }
    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable int id){
        extincteurService.deleteById(id);
    }

    @PatchMapping(path = "/{id}")
    public Extincteur updatePassword(@PathVariable int id,@RequestBody ExtincteurModels e){
        return extincteurService.updatePassword(id,e);

    }
}