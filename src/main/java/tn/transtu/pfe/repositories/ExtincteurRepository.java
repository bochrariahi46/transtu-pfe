package tn.transtu.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tn.transtu.pfe.entities.Extincteur;

import java.util.List;

public interface ExtincteurRepository extends JpaRepository<Extincteur,Integer> {
    @Query(value = "select e from Extincteur e where e.ref = :ref")
    List<Extincteur> findByRef(@Param(value="ref") int ref);
}
