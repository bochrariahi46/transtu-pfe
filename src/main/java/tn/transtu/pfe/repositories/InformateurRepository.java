package tn.transtu.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.transtu.pfe.entities.Informateur;

public interface InformateurRepository extends JpaRepository<Informateur,Integer> {
}
