package tn.transtu.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.transtu.pfe.entities.User;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUsername(String username);
}
