package tn.transtu.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tn.transtu.pfe.entities.Utilisateur;

import java.util.List;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {
    @Query(value = "select u from Utilisateur u where u.matricule = :matricule")
    List<Utilisateur> findByMat(@Param(value="matricule") int matricule);
}
