package tn.transtu.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tn.transtu.pfe.entities.Conducteur;
import tn.transtu.pfe.entities.Receveur;

import java.util.List;

public interface ReceveurRepository extends JpaRepository<Receveur, Integer> {
    @Query(value = "select r from Receveur r where r.matricule = :matricule")
    List<Receveur> findByMat(@Param(value="matricule") int matricule);
}
