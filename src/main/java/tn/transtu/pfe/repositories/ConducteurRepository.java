package tn.transtu.pfe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tn.transtu.pfe.entities.Conducteur;

import java.util.List;

public interface ConducteurRepository extends JpaRepository<Conducteur, Integer> {
    @Query(value = "select c from Conducteur c where c.matricule = :matricule")
    List<Conducteur> findByMat(@Param(value="matricule") int matricule);
}
