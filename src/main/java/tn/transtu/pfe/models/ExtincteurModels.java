package tn.transtu.pfe.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExtincteurModels {
    private String ref;
    private String emplacement;
    private String dateEnt;

}
