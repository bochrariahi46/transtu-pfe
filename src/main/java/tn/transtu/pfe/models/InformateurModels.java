package tn.transtu.pfe.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InformateurModels {
    private String adjective;
    private String firstName;
    private String lastName;
    private int tel;
}
