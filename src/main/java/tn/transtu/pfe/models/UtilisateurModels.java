package tn.transtu.pfe.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurModels {
    private int matricule;
    private String role;
    private String firstName;
    private String lastName;
    private int tel;
    private String username;
    private String password;
}
